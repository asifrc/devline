_Bismillah_

#DevLine

Generates a timeline for a project's development based on data pulled from
the following API's:

+ BitBucket
+ Trello
+ GitHub (maybe)
+ AgileZen (maybe not)

Initially this will just aggregate all the data from the API's and organize it
into a single JSON response that you can do whatever with.

Later I will likely try to make some sort of UI implementing this meta-api.